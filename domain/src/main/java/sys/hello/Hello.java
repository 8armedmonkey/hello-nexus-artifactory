package sys.hello;

public final class Hello {

    public void say() {
        System.out.println("Hello!");
    }

    public void sayTwice() {
        System.out.println("Hello! Hello!");
    }

    public void sayThrice() {
        System.out.println("Hello! Hello! Hello!");
    }

}
