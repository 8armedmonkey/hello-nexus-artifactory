package sys.hello;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

public class TestHello {

    private Hello hello;
    private OutputStream outputStream;

    @Before
    public void setUp() {
        hello = new Hello();
        System.setOut(new PrintStream(outputStream = new ByteArrayOutputStream()));
    }

    @Test
    public void say_helloPrintedToSystemOut() {
        hello.say();
        assertTrue(outputStream.toString().trim().contains("Hello"));
    }

    @Test
    public void sayTwice_helloTwicePrintedToSystemOut() {
        hello.sayTwice();
        assertTrue(outputStream.toString().trim().matches("(?:Hello.*?){2}"));
    }

    @Test
    public void sayThrice_helloThricePrintedToSystemOut() {
        hello.sayThrice();
        assertTrue(outputStream.toString().trim().matches("(?:Hello.*?){3}"));
    }

    @After
    public void tearDown() throws IOException {
        outputStream.close();
        System.setOut(null);
    }

}
